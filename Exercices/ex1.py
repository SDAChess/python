#Caracter Input : Exercice 1: https://www.practicepython.org/exercise/2014/01/29/01-character-input.html
#Simon Scatton

import os 
import math

name = input("Quel est votre nom ?")
age = input("Quel est votre âge ?")

age = int(age)
name = str(name)

year = 2018+(100-age)

print("Bonjour {0}, vous aurez 100 ans en {1}".format(name, year))
