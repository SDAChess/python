#Odd or even : Exercice 2 : https://www.practicepython.org/exercise/2014/02/05/02-odd-or-even.html
#Simon Scatton

import os


#Tester pour un nombre pair.
# number = int(input("Quel est le nombre ? \n"))

#if number % 4 == 0:
#    print("Le nombre est divisible par 4")
#elif number % 2 == 0: 
#    print("Le nombre est pair.")
#else:
#    print("Le nombre est impair.")

number1 = int(input("Quel est le nombre à diviser ? \n"))
number2 = int(input("Quel est le diviseur ? \n"))

if number1 % number2 == 0:
    print("{0} divise bien {1}".format(number2, number1))
else:
    print("{0} ne divise pas {1}".format(number2, number1))
