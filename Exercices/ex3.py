#List Less Than Ten : Exercice 3 : https://www.practicepython.org/exercise/2014/02/15/03-list-less-than-ten.html
#Simon Scatton

import os 

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]

tri = int(input("Quel est le nombre maximal pour le tri ?\n"))

new_list = []

for i in a:
    if i < tri:
        new_list.append(i)

print(new_list)