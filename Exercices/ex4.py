#Divisors : Exercice 4 : https://www.practicepython.org/exercise/2014/02/26/04-divisors.html
#Simon Scatton

import os 

number = int(input("Quel est le nombre désiré ? \n"))

possible_divisors = range(1, number+1)

divisors = []

for i in possible_divisors:
    if number % i == 0:
        divisors.append(i)

print("Les diviseurs possibles de {0} sont {1}".format(number, divisors))

