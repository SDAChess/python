#List Overlap : Exercice 5 : https://www.practicepython.org/exercise/2014/03/05/05-list-overlap.html
#Simon Scatton

import os
import random 

a = range(1, random.randint(1,30))

b = range(1, random.randint(10,40))

c = set(a).intersection(b)

print(c)