#String Lists : Exercice 6 : https://www.practicepython.org/exercise/2014/03/12/06-string-lists.html
#Simon Scatton

import os 
print("Testeur de Palyndrome : \n")
word = input("Quel est le mot à tester ? \n")

size = len(word)

word_2 = word[::-1]

if word == word_2:
    print("C'est un palyndrome !")
else:
    print("Ce n'est pas un palyndrome.")