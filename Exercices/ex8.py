#Rock Paper Scissors : Exercice 8 : https://www.practicepython.org/exercise/2014/03/26/08-rock-paper-scissors.html
#Simon Scatton

import os 

def clear():
    os.system('cls' if os.name == 'nt' else clear)
    return ''

game = True

clear()

Player1 = input("Quel est le nom du joueur 1 ? \n")
Player2 = input("Quel est le nom du joueur 2 ? \n")

score_player1 = 0
score_player2 = 0

count = 0

while game:
    clear()
    game_length = int(input("Combien de manches voulez vous jouer ? \n"))
    clear()
    while count < game_length:
        while True:
            move1 = input(Player1 + " : Pierre, Papier ou Ciseaux ? \n") #Input du joueur 1
            if move1.lower() == "ciseaux" or move1.lower() == "pierre" or move1.lower() == "papier":
                break
            else: 
                print("Ce n'est pas une option valide.")

        clear()

        while True:
            move2 = input(Player2 + " : Pierre, Papier ou Ciseaux ? \n") #Input du joueur 2
            if move2.lower() == "ciseaux" or move2.lower() == "pierre" or move2.lower() == "papier":
                break
            else: 
                print("Ce n'est pas une option valide.")

        clear() 

        if move1.lower() == "papier" and move2.lower() == "ciseaux": #On teste si J1 fait papier et J2 fait ciseaux.
            score_player2 += 1
        elif move1.lower() == "papier" and move2.lower() == "pierre": #On teste si J1 fait papier et J2 fait pierre.
            score_player1 += 1
        elif move1.lower() == "pierre" and move2.lower() == "ciseaux": #On teste si J1 fait pierre et J2 fait ciseaux.
            print(Player1 + "gagne !")
        elif move1.lower() == "pierre" and move2.lower() == "papier": #On teste si J1 fait pierre et J2 fait papier.
            score_player2 += 1
        elif move1.lower() == "ciseaux" and move2.lower() == "pierre": #On teste si J1 fait ciseaux et J2 fait pierre.
            score_player2 += 1
        elif move1.lower() == "ciseaux" and move2.lower() == "papier": #On teste si J1 fait ciseaux et J2 fait papier.
            score_player1 += 1
        count += 1

    if score_player1 > score_player2:
        print(Player1 + " gagne la partie !")
    elif score_player1 < score_player2:
        print(Player2 + " gagne la partie !")
    elif score_player1 == score_player2:
        print("Egalité parfaite !")

    while True: 
        rejouer = input("Voulez vous rejouer ? oui/non \n")
        if rejouer == "non":
            exit()            
        elif rejouer == "oui":
            print("La partie continue !")
            break
        else:
            print("Vous avez mal tapé le mot")
    count = 0