#Guessing Game One : Exercice 9 : https://www.practicepython.org/exercise/2014/04/02/09-guessing-game-one.html
#Simon Scatton

import os 
import random

def clear():
    os.system('cls' if os.name == 'nt' else clear)
    return ''

count = 0
game = True

while game:
    number = random.randint(0,9)
    guess = 10
    while guess!=number:
        guess = int(input("Quel est votre proposition ? \n"))
        if type(guess) != int or guess > 9 or guess < 0:
            print("Entrer un compris entre 0 et 9")
        else:
            count += 1
    print("Bravo ! Vous avez trouvé le numéro en {0} coups \n".format(count))
    while True: 
        rejouer = input("Voulez vous rejouer ? oui/non \n")
        if rejouer == "non":
            exit()            
        elif rejouer == "oui":
            print("La partie continue !")
            game = True
            clear()
            break
        else:
            print("Vous avez mal tapé le mot")
    count = 0