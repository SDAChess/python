#Simon Scatton : Snake game for Genetic Algorithm
#Based on  Christian Thompson's code

#  _____             _        
#/  ___|           | |       
#\ `--. _ __   __ _| | _____ 
# `--. \ '_ \ / _` | |/ / _ \
#/\__/ / | | | (_| |   <  __/
#\____/|_| |_|\__,_|_|\_\___|
                                
                            

import turtle 
import time
import random

#Game Global Variables

delay = 0.1

#Window setup and configuration.
window = turtle.Screen()
window.title("Snake")
window.bgcolor("white")
window.setup(width = 600, height = 600)
window.tracer(0)

#Snake Head 
head = turtle.Turtle()
head.speed(0)
head.shape("square")
head.color("black")
head.penup()
head.goto(0, 0)
head.direction = "stop"

#Food
food = turtle.Turtle()
food.speed(0)
food.shape("square")
food.color("green")
food.penup()
food.goto(0, 100)

#Bodyparts 

bodypart = []

#Functions
#Movement
def go_up():
    head.direction = "up"
def go_down():
    head.direction = "down"
def go_right():
    head.direction = "right"
def go_left():
    head.direction = "left"

def move():
    if head.direction == "up": 
        head.sety(head.ycor() + 20)
    if head.direction == "down":
        head.sety(head.ycor() - 20)        
    if head.direction == "right":
        head.setx(head.xcor() + 20)
    if head.direction == "left":
        head.setx(head.xcor() - 20)

#Used to generate new food throughout the game.
def food_dynamic():
    if head.distance(food) < 20:
        #Change the food position.
        food.goto(random.randint(-290, 290), random.randint(-290, 290))
        #Adds a bodypart to the snake
        new_bodypart = turtle.Turtle()
        new_bodypart.speed(0)
        new_bodypart.shape("square")
        new_bodypart.color("black")
        new_bodypart.penup()
        bodypart.append(new_bodypart)

#Bindings
#Binds key to move the snake around the map.
window.listen()
window.onkeypress(go_up , "Up")
window.onkeypress(go_down, "Down")
window.onkeypress(go_left, "Left")
window.onkeypress(go_right, "Right")

#Main Game Loop
while True:
    window.update()
    move()
    time.sleep(delay)
    food_dynamic() #Add foods and moves food when touched by head().

    #Moving the bodypart at the bottom of the last bodypart
    for index in range(len(bodypart)-1, 0, -1):
        bodypart[index].goto(bodypart[index-1].xcor(), bodypart[index-1].ycor())
    if len(bodypart) > 0:   #Moving the first bodypart to the head to create the chain.
        bodypart[0].goto(head.xcor(), head.ycor()) 

    #Wall collision checking and game reset feature.
    if head.xcor()<-290 or head.ycor()<-290 or head.xcor()>290 or head.ycor()>290:
        time.sleep(1)
        head.goto(0, 0)
        head.direction = "stop"
        for bodypart in bodypart:
            bodypart.goto(1000,1000)
        bodypart = []
    #Self collision checking and reseting the game.
    for bodypart in bodypart:
        if bodypart.distance(head) < 20:
            time.sleep(1)
            bodypart.goto(1000,1000)
            head.goto(0,0)
            head.direction = "stop"
        bodypart = []

window.mainloop()
