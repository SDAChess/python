# -*-coding:UTF-8 -*

import os # On importe le module os qui dispose de variables 
          # et de fonctions utiles pour dialoguer avec votre 
          # système d'exploitation
from random import randrange #On apporte le module pour générer le nombre aléatoire
import math # On importe le module maths


win = False
balance = 0.0 #Argent sur le compte du joueur
deposit = input("Combien voulez vous mettre sur votre compte ? ")
deposit = int(deposit)
balance = deposit

os.system('cls' if os.name == 'nt' else 'clear')

mise = input("Combien voulez vous miser ? \n")
mise = int(mise)
while mise > balance:
    print("Vous n'avez pas assez d'argent \n")
    deposit = input("Combien voulez vous ajouter ? \n")
    deposit = int(deposit)
    balance = balance + deposit
    mise = input("Combien voulez vous miser ? \n")
    mise = int(mise)
else:
    pass

balance = balance - mise

bet = input("Sur quel numéro voulez vous miser ? ")
bet = int(bet)




while bet<0 or bet>50:
    print("Le numéro n'est pas compatible. Veuillez choisir un numéro entre 0 et 50")
    bet = input("Sur quel numéro voulez vous miser ?")
    bet = int(bet)

roll = randrange(50)

if roll % 2 == 0 and bet % 2 == 0 or roll == bet:
    win = True
    print("Vous avez gagné ! \n")
else:
    win = False
    print("Vous avez perdu... \n")

if win and roll == bet:
    balance = balance + 3 * mise
elif win: 
    balance = balance + 0.5 * mise + mise

print("Vous avez maintenant {0} $ sur votre compte.".format(balance))